let sheet =
  Opium.App.get "/sheet/:name" (fun req ->
      let name = Opium.Router.param req "name" in
      Libapi.Sheet_API.(html_sheet name) |> Opium.Response.of_html |> Lwt.return)

let static_resources =
  Opium.Middleware.static_unix ~local_path:"./static" ~uri_prefix:"/static" ()

let node_modules_resources =
  Opium.Middleware.static_unix ~local_path:"./node_modules"
    ~uri_prefix:"/node_modules" ()

let () =
  Opium.App.empty
  |> Opium.App.middleware static_resources
  |> Opium.App.middleware node_modules_resources
  |> sheet |> Opium.App.run_command
