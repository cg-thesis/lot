.PHONY: build clean check install uninstall

DUNE=dune

build: lib/main/lot_client.ml
	dune build @install
	dune build ./lib/main/lot_client.bc.js @install

install:
	dune install
	ln -f _build/default/lib/main/lot_client.bc.js static/
	yarn exec webpack build

uninstall:
	dune uninstall
	unlink static/lot_client.bc.js

check: install
	make -C tests

watch:
	dune build --watch

clean:
	dune clean

dev-deps:
	opam exec -- opam install --deps-only --assume-depexts -y ./dev-deps
