import { TezosToolkit } from "@taquito/taquito";
import * as signer from "@taquito/signer";
import * as monaco from "monaco-editor";
import Handsontable from "handsontable";

let Tezos = new TezosToolkit("https://jakartanet.ecadinfra.com");
window.Tezos = Tezos;

let faucet_key = {
  pkh: "tz1N4WqdTPGBWLTqV8dNPN3ndtiQRCAQEVAS",
  mnemonic: [
    "blanket",
    "bless",
    "chest",
    "blame",
    "wealth",
    "strong",
    "vehicle",
    "return",
    "close",
    "boost",
    "token",
    "vehicle",
    "fetch",
    "resist",
    "monkey",
  ],
  email: "cdaqouak.hsblhlea@teztnets.xyz",
  password: "MIV6I0YDk0",
  amount: "299415424644",
  activation_code: "a0565b4b9c0b082eaa5768c9f9e5523f4472ce20",
};

let in_memory_signer = function () {
  return signer.InMemorySigner.fromFundraiser(
    faucet_key.email,
    faucet_key.password,
    faucet_key.mnemonic.join(" ")
  );
};
window.in_memory_signer = in_memory_signer;

let set_signer = function () {
  Tezos.setProvider({ signer: in_memory_signer() });
};
window.set_signer = set_signer;

let get_balance = function () {
  Tezos.tz
    .getBalance(faucet_key.pkh)
    .then((balance) => console.log("hello " + balance.toNumber() / 1000))
    .catch((e) => console.log(JSON.stringify(e)));
};
window.get_balance = get_balance;

let call_contract = function (k) {
  Tezos.contract
    .transfer({
      to: k,
      amount: 1,
      parameter: { entrypoint: "default", value: { prim: "Unit" } },
    })
    .then((op) => {
      op.confirmation(1).then(() => console.log(op.hash));
    })
    .catch((error) => {
      console.log(JSON.stringify(error, null, 2));
    });
};
window.call_contract = call_contract;

var ed;
var editor = monaco.editor.create(document.getElementById("lisa-editor"), {
  value: [].join("\n"),
  language: "cameligo",
});
ed = editor;

var sheet = [[], []];
window.sheet = sheet;

var export_plugin;
window.export_plugin = export_plugin;

var hot;
window.hot = hot;

let container = document.getElementById("table");

let discard_autofill = function (changes, source) {
  if (source === "Autofill.fill") return false;
};
window.discard_autofill = discard_autofill;

let formulaBar = document.getElementById("formula-bar");

let config = {
  licenseKey: "non-commercial-and-evaluation",
  // stretchH: "all",
  autoWrapRow: false,
  rowHeaders: true,
  colHeaders: true,
  filters: true,
  fillHandle: {
    direction: "vertical",
  },
  dropdownMenu: [
    "row_above",
    "row_below",
    "col_left",
    "col_right",
    "---------",
    "remove_col",
    "remove_row",
    "---------",
    "make_read_only",
    "---------",
    "alignment",
  ],
  selectionMode: "multiple",
  beforeChange: discard_autofill,
  rowHeaders: function (index) {
    if (index === 0) {
      return "";
    } else {
      return index;
    }
  },
  // beforeRefreshDimensions() { return false; },
  width: "100%",
  afterSelection: function (r, c, r2, c2) {
    const cellValue = hot.getDataAtCell(r, c);
    formulaBar.value = cellValue;
  },
  afterChange: function (changes, source) {
    if (changes) {
      const [row, prop, oldValue, newValue] = changes[0];
      if (source === "edit" || source === "paste") {
        formulaBar.value = newValue;
      }
    }
  },
};

window.config = config;
let set_before_autofill = function (f) {
  config["beforeAutofill"] = f;
};
window.set_before_autofill = set_before_autofill;

let get_sheet = function () {
  let range = hot.getSelectedRange()[0];
  let tc = range.to.col;
  let tr = range.to.row;
  return export_plugin.exportAsString("csv", {
    bom: false,
    columnDelimiter: "|",
    columnHeaders: false,
    exportHiddenColumns: false,
    exportHiddenRows: false,
    rowDelimiter: "\n",
    rowHeaders: false,
    range: [0, 0, tr, tc],
  });
};
window.get_sheet = get_sheet;

let get_cell = function (r, c) {
  return hot.getDataAtCell(r, c);
};
window.get_cell = get_cell;

let get_defs = () => ed.getValue();
window.get_defs = get_defs;

let set_defs = (s) => ed.setValue(s);
window.set_defs = set_defs;

let set_sheet = function (s) {
  hot.populateFromArray(0, 0, s);
  hot.refreshDimensions();
};
window.set_sheet = set_sheet;

let set_after_edit = function (f) {
  config["afterBeginEditing"] = function (row, col) {
    f(col, row);
  };
};
window.set_after_edit = set_after_edit;

let set_cell_data_edit = function (f) {
  let handler = function (changes, source) {
    if (source === "edit") {
      f(changes[0][3]);
    }
  };
  Handsontable.hooks.once("afterSetDataAtCell", handler, hot);
};
window.set_cell_data_edit = set_cell_data_edit;

let load_filter = function () {
  window.set_sheet([
    ["x", "filter"],
    ["", "=((init x) is_even) 0"],
    ["", "=IF(is_even x[], x[], filter[-1])"],
  ]);
  window.set_defs(
    "\
let mod = fun n m -> n - (m * (n / m))\n\
\n\
let is_even = fun n -> 0 = mod n 2\n\
\n\
let id_int : int -> int = fun x -> x\n\
\n\
let pred = fun x -> x-1\n\
\n\
let current : (int stream) -> int = fun x -> x[i.id_int i]\n\
\n\
let pre : (int stream) -> int = fun x -> x[i.pred i]\n\
\n\
let init = fun x f alt -> if (f (current x)) then (current x) else alt"
  );
};

let load_invest = function () {
  window.set_sheet([
    [
      "from ",
      " amount ",
      " operations",
      "active_capital",
      "available_investment",
      "ledger",
    ],
    [
      "",
      "",
      " =MAP.EMPTY()",
      " =IF(AND(from[] = 0, NOT(amount[] = 0)), 0, amount[])",
      " =IF(from[]=0, 0, amount[])",
      " =IF(from[] = 0, MAP.EMPTY(), MAP(PAIR(from[], amount[])))",
    ],
    [
      "",
      "",
      " =IF(AND(from[] = 0, amount[] = 0), MAP(PAIR(from[], available_investment[-1])), IF(from[]=0, MAP.MAP(((earnings amount[]) active_capital[-1]), ledger[-1]), MAP.EMPTY())) ",
      " =IF(AND(from[] = 0, NOT(amount[] = 0)), 0, amount[] + active_capital[-1]) ",
      " =IF(AND(from[]=0, amount[]=0), 0, IF(from[] = 0, available_investment[-1], available_investment[-1] + amount[]))",
      " =IF(from[] = 0,IF(amount[] = 0, ledger[-1], MAP.EMPTY()), MAP.UPD(from[], amount[], update_ledger, ledger[-1]))",
    ],
  ]);
  window.set_defs(
    "\
let update_ledger = (fun key ->\n\
                      (fun value ->\n\
                        (fun entry ->\n\
                          if proj 0 2 entry = key then\n\
                  	        (key, value + proj 1 2 entry)\n\
                	        else entry)))\n\
\n\
let earnings =\n\
  (fun global_earnings ->\n\
    (fun capital ->\n\
      (fun entry ->\n\
        (proj 0 2 entry, ((((1000000 * (proj 1 2 entry)) + capital/2)/capital) * global_earnings)/1000000))))"
  );
};

let load_nat = function () {
  window.set_sheet([["nat"], ["0"], ["=(succ nat[-1])"]]);
  window.set_defs("let succ = fun x -> x+1");
};

var selected_cell = {};
window.selected_cell = selected_cell;

let get_selected_cell = function () {
  return selected_cell;
};
window.get_selected_cell = get_selected_cell;

let set_selected = function (row, col, r, c, _o, _f) {
  console.log(row);
  selected_cell = { row, col };
  console.log(selected_cell);
  return selected_cell;
};
window.set_selected = set_selected;

let new_table = function (s) {
  config["data"] = s;
  hot = new Handsontable(container, config);
  export_plugin = hot.getPlugin("exportFile");
  document.addEventListener("DOMContentLoaded", function () {
    let removeFirstRow = document.getElementById("removeFirstRow");
    let removeFirstColumn = document.getElementById("removeFirstColumn");

    let addRow = document.getElementById("addRow");

    let addColumn = document.getElementById("addColumn");

    let dropdownButton = document.querySelector(".dropdown-button");
    let dropdownContent = document.querySelector(".dropdown-content");

    let options = dropdownContent.querySelectorAll("a"); // Select all <a> tags within .dropdown-content

    if (!dropdownButton | !dropdownContent) {
      console.error("Dropdown button or content not found");
      return;
    }

    // Function to handle option clicks
    function handleOptionClick(event) {
      event.preventDefault(); // Prevent default link behavior
      let optionText = event.target.textContent;
      console.log(`Clicked on ${optionText}`);
      if (optionText === "Nat") {
        load_nat();
      } else if (optionText === "Filter") {
        load_filter();
      } else if (optionText === "Invest") {
        load_invest();
      }
    }

    // Add click event listener to each dropdown option
    options.forEach((option) => {
      option.addEventListener("click", handleOptionClick);
    });

    dropdownButton.addEventListener("click", () => {
      // Toggle the display property of the dropdown content
      if (dropdownContent.style.display === "block") {
        dropdownContent.style.display = "none";
      } else {
        dropdownContent.style.display = "block";
      }
    });

    // Close the dropdown if the user clicks outside of it
    window.addEventListener("click", (event) => {
      if (!event.target.matches(".dropdown-button")) {
        if (dropdownContent.style.display === "block") {
          dropdownContent.style.display = "none";
        }
      }
    });

    Handsontable.dom.addEvent(removeFirstRow, "click", function () {
      hot.alter("remove_row", 0);
    });

    Handsontable.dom.addEvent(removeFirstColumn, "click", function () {
      hot.alter("remove_col", 0);
    });

    Handsontable.dom.addEvent(addRow, "click", function () {
      hot.alter("insert_row", hot.countRows());
    });

    Handsontable.dom.addEvent(addColumn, "click", function () {
      hot.alter("insert_col", hot.countCols());
    });

    hot.addHook("afterSelection", set_selected);

    formulaBar.addEventListener("change", function () {
      let selected_cell = get_selected_cell();
      if (selected_cell) {
        hot.setDataAtCell(
          selected_cell.row,
          selected_cell.col,
          formulaBar.value
        );
      }
    });

    formulaBar.addEventListener("keydown", function (event) {
      if (event.key === "Enter") {
        event.preventDefault();
        let selected_cell = get_selected_cell();
        if (selected_cell) {
          hot.setDataAtCell(
            selected_cell.row,
            selected_cell.col,
            formulaBar.value
          );
        }
      }
    });
  });
};
window.new_table = new_table;
