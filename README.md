# LoT, Lisa on Table

LoT is a web service that implements a spreadsheet application as a
web app based on the [Lisa](https://gitlab.com/cg-thesis/lisa)
programming language. It aims to be a concrete implementation with
support for native structured spreadsheets with Lisa macros.

## Introduction

LoT is a proof of concept that implements spreadsheet software as a
web app based on the [Lisa](https://gitlab.com/cg-thesis/lisa)
programming language. It aims to be a concrete implementation of
native structured spreadsheets with support for Lisa scripts,
showcasing how a spreadsheet can be enhanced and automated using this
language.

Disclaimer: this software comes a is and it's in a very early
developpement phase. You should expect it to be buggy and practically
not ready for use.

## Features

- **Interactive Spreadsheets**: A fully functional spreadsheet
  interface that allows users to perform typical spreadsheet
  operations.
- **Lisa Text Editor**: A text editor integrated into the web app for
  developing extensions and scripts using the Lisa language.
- **Web Service**: The entire system is bundled as a web service,
  making it accessible through a web browser and simplifying
  deployment and access.

## Installation

To set up the development environment, follow these steps:

1. **Run the `configure` script:**
   ```bash
   ./configure
   eval $(opam env)
   ```

2. **Build the server package and bundle the JS scripts with Webpack:**
   ```bash
   make
   ```

3. **Install all runtime web app dependencies:**
   ```bash
   make install
   ```

This will also install the `lisa` binary.

Ensure that you have the necessary dependencies and tools installed on
your system, namely Opam version 2.0 higher and Yarn.

## Usage

Once the installation is complete, you can start the web service and
access the application as follows:

1. **Launch the web service:**
   ```bash
   lot &
   ```

2. **Open your web browser and navigate to:** ```
   http://localhost:3000/sheet/name ``` Replace `name` with the name
   of the spreadsheet document you want to open.

You should now see the interactive spreadsheet and be able to use the
features provided by the application.

## Contributing

To contribute to the LoT project, follow these steps:

1. **Install Development Dependencies:**
   Run the following command to install necessary development dependencies:
   ```bash
   make dev-deps
   ```

2. **Fork the Repository:** Fork the project repository on GitHub.

3. **Create a New Branch:**
   Create a new branch for your changes:
   ```bash
   git checkout -b feature-branch
   ```

4. **Make Your Changes:**
   Implement your changes and test them thoroughly.

5. **Commit Your Changes:**
   Commit your changes with a descriptive message:
   ```bash
   git commit -m 'Add feature or fix issue'
   ```

6. **Push to Your Fork:**
   Push your changes to your forked repository:
   ```bash
   git push origin feature-branch
   ```

7. **Submit a Merge Request:** Open a merge request (pull request) on
   GitHub to merge your changes into the main repository.

8. **Use the Issue Tracker:**
   For reporting bugs or requesting features, use the project's issue tracker.

Please ensure your code adheres to the project's coding standards and
guidelines.

## Contact

For any questions or inquiries, please use the project's issue system on Gitlab.

Project Link: [https://gitlab.com/cg-thesis/lot](https://gitlab.com/cg-thesis/lot).
```
