open Tyxml.Html

let content name =
  div
    ~a:[ a_class [ "content" ] ]
    [
      h1 [ txt name ];
      txt
        "Welcome to Lisa on Table.\n\
         To run a program, select all the cells of the program and drag down \
         the last line.";
    ]

let bootstrap_div bclass inner_div = div ~a:[ a_class [ bclass ] ] inner_div

let table =
  div
    ~a:
      [
        a_id "table";
        a_style
          (Xml.uri_of_string "overflow:scroll; width: 500px; height: 600px;");
      ]
    []

let drop_down =
  div
    ~a:[ a_class [ "dropdown" ] ]
    [
      button ~a:[ a_class [ "dropdown-button" ] ] [ txt "Select example" ];
      div
        ~a:[ a_class [ "dropdown-content" ] ]
        [
          a ~a:[ a_href "#" ] [ txt "Nat" ];
          a ~a:[ a_href "#" ] [ txt "Filter" ];
          a ~a:[ a_href "#" ] [ txt "Invest" ];
        ];
    ]

let editor =
  div
    ~a:
      [
        a_id "lisa-editor";
        a_style
          (Xml.uri_of_string
             "width: 500px; height: 600px; border: 1px solid grey");
      ]
    []

let formula_bar =
  div
    ~a:[ a_class [ "formula-bar-container" ] ]
    [
      input
        ~a:
          [
            a_input_type `Text;
            a_id "formula-bar";
            a_class [ "formula-bar" ];
            a_placeholder "Enter formula or value";
          ]
        ();
    ]

let sheet_title sheet_name = title (txt sheet_name)

let script_module path =
  script
    ~a:
      [
        a_mime_type (Xml.uri_of_string "module"); a_src (Xml.uri_of_string path);
      ]
    (txt "")

let script path = script ~a:[ a_src (Xml.uri_of_string path) ] (txt "")
let main_script = script "/static/main.o.js"

let bootstrap_css =
  link ~href:"/node_modules/bootstrap/dist/css/bootstrap.css"
    ~rel:[ `Stylesheet ]
    ~a:[ a_media [ `Screen ] ]
    ()

let style_css =
  link ~href:"/static/style.css"
    ~rel:[ `Stylesheet ]
    ~a:[ a_media [ `Screen ] ]
    ()

let handsontable_script = script "/static/handsontable.full.min.js"
let taquito_script = script "/static/taquito.min.js"
let taquito_signer_script = script_module "/static/taquito-signer.es6.js"

let handsontable_css =
  link ~href:"/static/handsontable.full.min.css"
    ~rel:[ `Stylesheet ]
    ~a:[ a_media [ `Screen ] ]
    ()

let monaco_script = script "/node_modules/monaco-editor/min/vs/loader.js"
let lisacsv_script = script "/static/lot_client.bc.js"

let button vclass vid vtext =
  button ~a:[ a_class vclass; a_id vid ] [ txt vtext ]

let in_text_button = button [ "intext-btn" ]
let remove_first_row_btn = in_text_button "removeFirstRow" "Remove first row"

let remove_first_column_btn =
  in_text_button "removeFirstColumn" "Remove first column"

let add_row_btn = in_text_button "addRow" "Add row"
let add_column_btn = in_text_button "addColumn" "Add column"
let run_btn = in_text_button "run" "Run"
let charset_utf8 = meta ~a:[ a_charset "utf-8" ] ()

let html_sheet name =
  html
    (head (sheet_title name)
       [ charset_utf8; bootstrap_css; handsontable_css; style_css ])
    (body
       [
         bootstrap_div "container-fluid" [ content name; drop_down ];
         remove_first_row_btn;
         remove_first_column_btn;
         add_row_btn;
         add_column_btn;
         (* run_btn; *)
         bootstrap_div "container-fluid"
           [
             bootstrap_div "row"
               [
                 bootstrap_div "col-md-6" [ formula_bar; table ];
                 bootstrap_div "col-md-6" [ editor ];
               ];
           ];
         main_script;
         lisacsv_script;
       ])

let new_sheet sheet_name =
  Format.asprintf "%a@." (pp ~indent:true ()) (html_sheet sheet_name)
