module Sheet_state = struct
  type proc = Stopped | Running of Lisa.Syntax.value

  and status = Pending | Resolved of string

  and input_promise = { mutable status : status }

  type t = {
    mutable ids : string list;
    mutable env : Lisa.Syntax.environment;
    mutable proc : proc;
    mutable source : Lisa.Syntax.program;
    mutable relative_defs : (int * Lisa.CSVCompiler.S.expression) option list;
    mutable index : int;
    mutable reset : bool;
    mutable autofill_target : int option;
    registered_inputs : (int * int, input_promise) Hashtbl.t;
  }

  let empty =
    {
      ids = [];
      env = [];
      proc = Stopped;
      source = [];
      relative_defs = [];
      index = -1;
      reset = true;
      autofill_target = None;
      registered_inputs = Hashtbl.create 256;
    }

  let set_ids state ids = state.ids <- ids

  let set_env state env = state.env <- env

  let set_proc state proc = state.proc <- proc

  let set_source state src = state.source <- src

  let set_relative_defs state timed_defs = state.relative_defs <- timed_defs

  let set_index state time = state.index <- time

  let set_autofill_target state time = state.autofill_target <- Some time

  let set_reset state value = state.reset <- value

  let toggle_reset state = state.reset <- not state.reset

  let check_registered state c r =
    Hashtbl.find_opt state.registered_inputs (c, r)

  let register_input state c r v =
    match Hashtbl.find_opt state.registered_inputs (c, r) with
    | Some i ->
        i.status <- Resolved v
    | None ->
        let i = { status = Resolved v } in
        Hashtbl.add state.registered_inputs (c, r) i

  let register_input_pending state c r =
    Hashtbl.add state.registered_inputs (c, r) { status = Pending; }
end
