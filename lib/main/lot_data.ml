open Js_of_ocaml
open Libutils.IO
module Lisa_IO = Lisa.IO

(* Conversions between spreadsheet and their JS reprenstation. *)

let js_to_string js = Js.Opt.case js (fun () -> "") Js.to_string

let js_to_sheet js = parse_sheet (Js.to_string js)

let js_to_header js = Lisa_IO.parse_binding (Js.to_string js)

let js_of_csv csv =
  Js.array
    (Array.of_list
       (List.map
          (fun row -> Js.array (Array.map Js.string (Array.of_list row)))
          csv))
