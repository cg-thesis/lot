open Libstate.Lot_state
open Lot_bindings
open Lisa
open Syntax
open Js_of_ocaml
open Lot_data
module Html = Dom_html

let add_handler id =
  let btn = Html.getElementById id in
  btn##.onclick :=
    Html.handler (fun _ ->
        print_endline "Clicked!";
        Js._false)

(* Summary: the client is in charge of parsing, compiling and
      interpreting spreadsheets. Spreadsheet are entered using
      Handsontable, a JS library.

      The client code reacts to some Handsontable events. Mainly, one for
      filling the table automagically and the second one to record
      interactive inputs.

      To fill the spreadsheet automagically uppon users request, the
      client sets a handler for the autofill Handsontable event. To
      autofill the spreadsheet the client will compile the spreadsheet to
      a Lisa program. Then, it will interpret the obtained Lisa program.
      To run the Lisa program the client will maintain a state per sheet
      called the runtime.

      The client also reacts to sheet's edits. For instance, some cells
      will trigger the runtime to update the spreadsheet. This is
      reflected in the Lisa evaluation as well. Whenever it depends on an
      input the evaluation will halt until an input is provided. The
      evaluation function of Lisa provides a hook mecanism to react to
      inputs. Whenever the evaluation requires and input, the client will
      start by checking if the runtime is aware of such an input. If the
      input exists, it is provided it to interpreter, if not, the runtime
      will wait for it. Whenever a waited input is provided, the client
      will try to resume the evaluation up to the input's coordinates.
      The client maintains provided inputs in the runtime and places
      callbacks on waited cells.

   **)

module Lot_runtime = struct
  (* Lot relies on Lisa to provide semantics for spreadsheet
     computations. A Lisa process is defined as an interactive program
     that is defined in two sperate components. A Lisa program and an
     interactive system capable controlling the evaluation of such
     program. The later is provided by the Lisa interpreter. It is a
     function called unfold that procededs to unfold top-level defined
     Lisa streams. The resulting value is used to update the
     spreadsheet. *)

  (* The type of columns and rows indices*)
  type pos = int

  (* The type of the coordinates of a cell *)
  and coords = { col : pos; row : pos }

  (* Exception thrown whenever the evaluation requires a missing input
     at col,row indices. *)
  exception No_input of coords

  (* Inital state.*)
  let state = Sheet_state.empty

  (* For a given column name, return the its position in the grid *)
  let get_column_pos label =
    List.mapi (fun pos label -> (label, pos)) state.ids |> List.assoc label

  (* This is the interactive input hook for the Lisa evaluator. The
     Lisa evaluation process and the interactive user interface are
     completely independent to each other. In order to comunicate
     inputs the Lisa interpreter accepts an input function that will
     qerry an input for a particular stream at a given index and
     resume, computation.

     However, this not quite compatible with the spreadsheet user
     interface, that requires some asyncrhony. Without it, the process
     would not be able to yield control to the user.

     Thus, the callback implements a simple asynchronous process.
     Provided, inputs are stored in memory. When a required input is
     needed, it is either stored or not provided yet. In the first
     case, the input callback returns the stored input. In the latter,
     an exception is raised, thus preempting control on the Lisa
     evaluation.

     A callback is placed on the required cell. Uppon cell editing the
     Lisa evaluation is resumed.

     The callback will try to look for the cell using the columns
     label and row number in the state. If the input is found it will
     use the input for the evaluation. If the input is missing it will
     raise a No_input exception. If the runtime not aware of the the
     referenced cell is an input, it will registered the cell as an
     non resolved input in the runtime's state. *)
  let async_input_callback (label, index) =
    let open Sheet_state in
    let row = index + 1 in
    let col = get_column_pos label in
    match check_registered state col row with
    | Some { status = Resolved exp } -> IO.parse_expression exp
    | Some { status = Pending } -> raise (No_input { col; row })
    | None ->
        register_input_pending state col row;
        raise (No_input { col; row })

  (* Main evaluation function. *)
  let eval env program =
    BigStepSemantics.eval async_input_callback env 0 program

  (* Launching the process is simply evaluating all the top-level
     definitions of a Lisa program.*)
  let process_launch src =
    List.iter (fun s -> print_endline (IO.print_binding s)) state.source;
    let env, (_, vals) =
      List.fold_left
        (fun prev def ->
          let env, _ = prev in
          eval env def)
        ([], (None, VUnit))
        src
    in
    (env, vals)

  (* A step function to progress the evaluation of one row. *)

  (* A generalisation of the take function to evaluate the first n
     rows.*)
  let take row process =
    let index = row - 1 in
    let open Sheet_state in
    try (index, BigStepSemantics.map_unfold index process)
    with No_input { col; row } ->
      register_input_pending state col row;
      (row - 1, process)

  (* The footer is the last row containing the expression to be
     repeated recursively. *)
  let state_init s ids program footer =
    let open Sheet_state in
    set_proc s Stopped;
    set_source s program;
    set_ids s ids;
    set_relative_defs s footer;
    set_index s 0;
    set_reset s false

  let source_missing () = state.source = []

  let state_update s index proc =
    let open Sheet_state in
    set_proc s (Running proc);
    set_index s index

  (* Generate and show the spreadsheet corresponding the current
     evalauation. *)
  let sheet_update proc =
    let open Sheet_state in
    CSV.csv_of_lisa state.ids proc state.relative_defs
    |> IO.pprint_sheet |> js_of_csv |> set_sheet

  let process_init s =
    let open Sheet_state in
    try
      let env, proc = process_launch s.source in
      set_env s env;
      set_index s 0;
      set_proc s (Running proc)
    with No_input _ -> ()

  let process_run_at row =
    let open Sheet_state in
    match state.proc with
    | Stopped ->
        let env, p = process_launch state.source in
        set_env state env;
        set_proc state (Running p);
        take row p
    | Running p -> take row p

  let init s ds =
    let ids, sheet = js_to_sheet s in
    let header = js_to_header ds in
    let footer = CSV.get_timed_defs sheet in
    let program = CSVCompiler.lisa_of_csv header sheet in
    state_init state ids program footer;
    process_init state

  let reset () =
    let open Sheet_state in
    toggle_reset state;
    set_index state 0;
    set_proc state Stopped;
    set_env state [];
    state.autofill_target <- None;
    process_init state

  (* Main autofill callback. Attempts to evaluate up to the requested
     row or wait for the necessary input.*)
  let autofill target_row =
    let open Sheet_state in
    set_autofill_target state target_row;
    try
      if state.reset then reset ();
      let index, proc = process_run_at target_row in
      set_index state index;
      sheet_update proc;
      if target_row = index + 1 then state.autofill_target <- None
    with No_input { col; row } -> register_input_pending state col row
end

(* Autofill callback ocaml binding. *)
let js_autofill_handler _ end_row _ =
  let open Lot_runtime in
  match Js.Optdef.to_option end_row with
  | Some n ->
      let js_target_row = n##.row in
      let target_row = int_of_float (Js.float_of_number js_target_row) in
      if source_missing () then init (get_sheet ()) (get_defs ());
      autofill target_row
  | None -> ()

(* Data edit callback: registers cell as input data and attempts
   evaluating up the to edited cell's row using autofill.*)
let data_edit col row str =
  let open Sheet_state in
  let open Lot_runtime in
  try
    if str <> "" then (
      register_input Lot_runtime.state col row str;
      let index, proc = process_run_at row in
      let () = set_index state index in
      sheet_update proc;
      match state.autofill_target with
      | Some target -> Lot_runtime.(autofill target)
      | _ ->
          state.autofill_target <- None;
          sheet_update proc)
  with No_input { col; row } -> register_input_pending state col row

(* Data edit callback ocaml binding. *)

let js_data_edit_handler col row jstr =
  let str = Js.to_string jstr in
  data_edit col row str

(* React to edits. If the cell is an expected input place a callback
   that is used only once to update the sheet when edit is
   done. If the cell is depended uppon update the
   spreadsheet. Otherwise do nothing*)
let after_edit_handler col row =
  let open Sheet_state in
  let state = Lot_runtime.state in
  if row <= 0 then ()
  else
    match check_registered state col row with
    | Some { status = Pending; _ } ->
        (* Expected input for the first time *)
        (* set_index state index; *)
        set_cell_data_edit (js_data_edit_handler col row)
    | Some { status = Resolved _ } ->
        (* Input modification *)
        toggle_reset state;
        set_cell_data_edit (js_data_edit_handler col row)
    | _ ->
        (* Source modification *)
        ()

(* Register autofill callback. *)
let _ = set_before_autofill js_autofill_handler

(* Register after edit callback. *)
let _ = set_after_edit after_edit_handler

(* Initialize a new handsontable table object *)
let _ = set_new_table ()

let _ =
  let open Lot_runtime in
  let btn = Html.getElementById "run" in
  btn##.onclick :=
    Html.handler (fun _ ->
        init (get_sheet ()) (get_defs ());
        Js._false)
