open Js_of_ocaml

module Combinators = struct
  (* Combinators to write function applications. *)
  let call f a = Js.Unsafe.fun_call (Js.Unsafe.eval_string f) a

  let cons_param x = [| Js.Unsafe.inject x |]

  let cons_param_bin x y = [| Js.Unsafe.inject x; Js.Unsafe.inject y |]

  let unit_param = [| Js.Unsafe.inject () |]

  let fun_param f = cons_param (Js.wrap_callback f)

  let js_app_unit f = call f unit_param

  let js_app f x = call f (cons_param x)

  let js_app_bin f x y = call f (cons_param_bin x y)

  let js_app_fun_param f g = call f (fun_param g)
end

open Combinators

let set_sheet s = ignore (js_app "window.set_sheet" s)

let get_sheet () : Js.js_string Js.t = js_app_unit "window.get_sheet"

let get_cell c r =
  let opt_str = Js.Optdef.to_option (js_app_bin "window.get_cell" r c) in
  Option.map Js.to_string opt_str

let get_defs () : Js.js_string Js.t = js_app_unit "window.get_defs"

let set_before_autofill c = js_app_fun_param "window.set_before_autofill" c

let set_after_edit c = js_app_fun_param "window.set_after_edit" c

let set_cell_data_edit c = js_app_fun_param "window.set_cell_data_edit" c

let set_new_table () =
  Js.Unsafe.fun_call
    (Js.Unsafe.js_expr "window.new_table")
    [|
      Js.Unsafe.inject
        (Js.array (Array.map Js.array (Array.make_matrix 10 10 (Js.string ""))));
    |]

(* js_app "new_table"
 *   (Js.array (Array.map Js.array (Array.make_matrix 10 10 (Js.string "")))) *)
