open Lisa

let parse_sheet s =
  let s = IO.parse_sheet s in
  let ids =
    List.map
      (function Libformula.Syntax.Var x :: _ -> x | _ -> assert false)
      s
  in
  (ids, s)
