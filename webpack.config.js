const path = require("path");
const webpack = require("webpack");

module.exports = {
  mode: "development",
  entry: "./static/main.js",
  output: {
    path: path.resolve(__dirname, "static"),
    filename: "main.o.js",
  },
  resolve: {
    modules: [path.resolve(__dirname, "node_modules")],
    fallback: {
      stream: require.resolve("stream-browserify"),
    },
  },
  plugins: [new webpack.ProvidePlugin({ Buffer: ["buffer", "Buffer"] })],
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"],
      },
      {
        test: /\.ttf$/,
        use: ["file-loader"],
      },
    ],
  },
  devtool: "inline-source-map",
};
